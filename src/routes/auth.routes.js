const {Router} = require("express");
const {authenticate} = require("../controllers/auth.controller");

const router = Router();

//POST -> iniciar sesión
router.post("/auth/signin", authenticate);


module.exports = router;