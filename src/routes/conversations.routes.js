const {Router} = require("express");
const conversationCtrl = require("../controllers/conversations.controller");
const {validateToken} = require("../middlewares/auth.middleware");
const router = Router();

//GET -> obtener todos los registros
router.get("/conversations", validateToken, conversationCtrl.getAll);
//GET -> obtener un registro por id
router.get("/conversations/:id", validateToken, conversationCtrl.getById);
//POST -> Agregar un registro
router.post("/conversations", validateToken, conversationCtrl.create);
//UPDATE -> Actualizar un registro
router.put("/conversations/:id", validateToken, conversationCtrl.update);
//DELETE -> Borrar un registro
router.delete("/conversations/:id", validateToken, conversationCtrl.delete);

//Relaciones con otros modelos
router.get("/conversations/:id/users", validateToken, conversationCtrl.conversationUsers);

router.get("/conversations/:id/participants", validateToken, conversationCtrl.conversationParticipants);

//Completar la siguiente ruta
router.delete("/conversations/:id/participants");

router.get("/conversations/:id/messages", validateToken, conversationCtrl.conversationMessages);

router.post("/conversations/:id/messages", validateToken, conversationCtrl.postMessage);

module.exports = router;